import sys
sys.path.insert(0, "..")
import logging
import time
import threading

try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()


from opcua import ua, uamethod, Server, Event, ObjectIds


class SubHandler(object):

    """
    Subscription Handler. To receive events from server for a subscription
    """

    def datachange_notification(self, node, val, data):
        print("Python: New data change event", node, val)

    def event_notification(self, event):
        print("Python: New event", event)


# method to be exposed through server

def func(parent, variant):
    ret = False
    if variant.Value % 2 == 0:
        ret = True
    return [ua.Variant(ret, ua.VariantType.Boolean)]


# method to be exposed through server
# uses a decorator to automatically convert to and from variants

@uamethod
def client_did_enter_job_run(parent):
    print "(Server) In client_did_enter_job_run()"
    def do_variable_change(server):
        print "(Server) Setting IN_JOB_START = 0"
        server.in_job_start_node.set_value(0)
    threading.Thread(target=do_variable_change, args=(server,)).start()

@uamethod
def client_did_exit_job_run(parent):
    print "(Server) In client_did_exit_job_run()"
    def do_variable_change(server):
        print "(Server) Setting IN_JOB_STOP = 0"
        server.in_job_stop_node.set_value(0)
    threading.Thread(target=do_variable_change, args=(server,)).start()

@uamethod
def client_did_enter_out_point_ready_with_result(parent, success):
    print "(Server) In client_did_enter_out_point_ready() with success flag {}".format(success)
    def do_variable_change(server):
        print "(Server) Setting IN_POINT_POS = 0"
        server.in_point_pos_node.set_value(0)
    threading.Thread(target=do_variable_change, args=(server,)).start()

@uamethod
def client_did_exit_out_point_ready(parent):
    print "(Server) In client_did_exit_out_point_ready()"
    def do_variable_change(server):
        print "(Server) Setting IN_JOB_STOP = 1"
        server.in_job_stop_node.set_value(1)
    threading.Thread(target=do_variable_change, args=(server,)).start()

class DummyRobot(Server):

    def run_measurement_sequence(self):

        print "(Server) Running measurement sequence..."
        time.sleep(2)

        print "(Server) Setting IN_JOB_START = 1"
        self.in_job_start_node.set_value(1)
        time.sleep(1)
        print '(Server) Robot is moving into position...'
        time.sleep(2)
        print '(Server) Robot in position!'
        print '(Server) Setting IN_POINT_POS = 1'
        self.in_point_pos_node.set_value(1)
        time.sleep(2)
        print '(Server) End'
 

    def setup(self):

        #server.disable_clock()
        #server.set_endpoint("opc.tcp://localhost:4841/freeopcua/server/")
        self.set_endpoint("opc.tcp://0.0.0.0:4841/freeopcua/server/")
        self.set_server_name("FreeOpcUa Example Server")

        # setup our own namespace
        uri = "http://examples.freeopcua.github.io"
        idx = self.register_namespace(uri)

        # get Objects node, this is where we should put our custom stuff
        objects = self.get_objects_node()

        # Create root OPC object to hold all our data
        aps_controller = objects.add_object(idx, "APSControllerObject")
        # Add variables
        self.in_job_start_node = aps_controller.add_variable(idx, "IN_JOB_START", 0)
        self.in_point_pos_node = aps_controller.add_variable(idx, "IN_POINT_POS", 0)
        self.in_job_stop_node = aps_controller.add_variable(idx, "IN_JOB_STOP", 0)
        # Add methods
        aps_controller.add_method(idx, "client_did_enter_job_run", client_did_enter_job_run, [], [])
        aps_controller.add_method(idx, "client_did_exit_job_run", client_did_exit_job_run, [], [])
        aps_controller.add_method(idx, "client_did_enter_out_point_ready_with_result", client_did_enter_out_point_ready_with_result, [ua.VariantType.Int32], [])
        aps_controller.add_method(idx, "client_did_exit_out_point_ready", client_did_exit_out_point_ready, [], [])

        # objects.add_variable(idx, "IN_POINT_POS", 0)
        # objects.add_variable(idx, "IN_JOB_STOP", 0)
        # objects.add_variable(idx, "job_data", "test job data string")
        # objects.add_variable(idx, "point_data", "test point data string")

        # # populating our address space
        # myfolder = objects.add_folder(idx, "myEmptyFolder")
        # myobj = objects.add_object(idx, "MyObject")
        # myvar = myobj.add_variable(idx, "MyVariable", 6.7)
        # myvar.set_writable()    # Set MyVariable to be writable by clients
        # myarrayvar = myobj.add_variable(idx, "myarrayvar", [6.7, 7.9])
        # myarrayvar = myobj.add_variable(idx, "myStronglytTypedVariable", ua.Variant([], ua.VariantType.UInt32))
        # myprop = myobj.add_property(idx, "myproperty", "I am a property")
        # mymethod = myobj.add_method(idx, "mymethod", func, [ua.VariantType.Int64], [ua.VariantType.Boolean])
        # multiply_node = myobj.add_method(idx, "multiply", multiply, [ua.VariantType.Int64, ua.VariantType.Int64], [ua.VariantType.Int64])
        

        # import some nodes from xml
        self.import_xml("custom_nodes.xml")

        # creating an event object
        # The event object automatically will have members for all events properties
        myevent = self.get_event_object(ObjectIds.BaseEventType)
        myevent.Message.Text = "This is my event"
        myevent.Severity = 300

if __name__ == "__main__":
    # optional: setup logging
    logging.basicConfig(level=logging.WARN)
    #logger = logging.getLogger("opcua.address_space")
    # logger.setLevel(logging.DEBUG)
    #logger = logging.getLogger("opcua.internal_server")
    # logger.setLevel(logging.DEBUG)
    #logger = logging.getLogger("opcua.binary_server_asyncio")
    # logger.setLevel(logging.DEBUG)
    #logger = logging.getLogger("opcua.uaprocessor")
    # logger.setLevel(logging.DEBUG)
    logger = logging.getLogger("opcua.subscription_service")
    logger.setLevel(logging.DEBUG)

    # now setup our server
    server = DummyRobot()
    server.setup()

    # starting!
    server.start()
    print("Available loggers are: ", logging.Logger.manager.loggerDict.keys())
    try:
        # enable following if you want to subscribe to nodes on server side
        #handler = SubHandler()
        #sub = server.create_subscription(500, handler)
        #handle = sub.subscribe_data_change(myvar)
        # trigger event, all subscribed clients wil receive it
        #myevent.trigger()

        embed()

    finally:
        server.stop()
