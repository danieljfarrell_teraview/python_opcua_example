import sys
sys.path.insert(0, "..")
import logging
import time
import threading

try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()


from opcua import Client
from opcua import ua

kInJobStartName = "IN_JOB_START"
kInPointPosName = "IN_POINT_POS"
kInJobStopName = "IN_JOB_STOP"

def notification_callback(client, node, val, data):

    root = client.get_root_node()
    obj = root.get_child(["0:Objects", "2:APSControllerObject"])
    name = node.get_display_name().to_string()
    if name == kInJobStartName and val == 1:
        #print "(Client) Handling change to... ", name
        print "(Client) Preparing for measurement... "
        # FIXME: Do job setup
        client.OUT_JOB_RUN = 1
        res = obj.call_method("2:client_did_enter_job_run")

    elif name == kInPointPosName and val == 1 and client.OUT_JOB_RUN == 1:
        print "(Client) Doing THz thickness measurement... "
        #print "(Client) Handling change to... ", name
        # FIXME: Do point measurement and when done tell server...
        client.OUT_POINT_RESULT = 1 # success (or fail)
        # FIXME: Prepare thickness report
        client.OUT_POINT_READY = 1
        res = obj.call_method("2:client_did_enter_out_point_ready_with_result", client.OUT_POINT_RESULT)

    elif name == kInPointPosName and val == 0 and client.OUT_JOB_RUN == 1 and client.OUT_POINT_READY == 1:
        print "(Client) THz thickness measurement completed... "
        #print "(Client) Handling change to... ", name
        # Retract point ready state
        client.OUT_POINT_READY = 0
        res = obj.call_method("2:client_did_exit_out_point_ready")

    elif name == kInJobStopName and val == 1 and client.OUT_JOB_RUN == 1 and client.OUT_POINT_READY == 0:
        #print "(Client) Handling change to... ", name
        print "(Client) Auto Paint Scan waiting for further instructions..."
        # Retract job run state
        client.OUT_JOB_RUN = 0
        res = obj.call_method("2:client_did_exit_job_run")

    else:
        pass


class SubHandler(object):

    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another 
    thread if you need to do such a thing
    """

    def __init__(self, client):
        super(SubHandler, self).__init__()
        self.client = client

    def datachange_notification(self, node, val, data):
        #print("Python: New data change event")
        thr = threading.Thread(target=notification_callback, args=(client, node, val, data)).start()

    def event_notification(self, event):
        print("Python: New event", event)
    
class APSClient(Client):

    def setup(self):

        self.OUT_JOB_RUN = 0
        self.OUT_POINT_RESULT = 0
        self.OUT_POINT_READY = 0

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        print("Root node is: ", root)
        objects = client.get_objects_node()
        print("Objects node is: ", objects)

        # Node objects have methods to read and write node attributes as well as browse or populate address space
        print("Children of root are: ", root.get_children())

        def subscribe_to_server_variable(variable_id):

            # Now getting a variable node using its browse path
            myvar = root.get_child(["0:Objects", "2:APSControllerObject", variable_id])

            # subscribing to a variable node
            handler = SubHandler(client)
            sub = client.create_subscription(500, handler)
            handle = sub.subscribe_data_change(myvar)
            time.sleep(0.1)

        subscribe_to_server_variable("2:IN_JOB_START")
        subscribe_to_server_variable("2:IN_POINT_POS")
        subscribe_to_server_variable("2:IN_JOB_STOP")

if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)
    #logger = logging.getLogger("KeepAlive")
    #logger.setLevel(logging.DEBUG)

    client = APSClient("opc.tcp://localhost:4841/freeopcua/server/")
    # client = APSClient("opc.tcp://admin@localhost:4841/freeopcua/server/") #connect using a user
    
    try:
        client.connect()
        client.setup()

        # we can also subscribe to events from server
        # sub.subscribe_events()
        # sub.unsubscribe(handle)
        # sub.delete()

        # calling a method on server
        #res = obj.call_method("2:multiply", 3, "klk")
        #print("method result is: ", res)

        embed()


    finally:
        client.disconnect()
